/*********
  Complete project details at http://randomnerdtutorials.com  
*********/
//Libraries
#include <SoftwareSerial.h>
#include <DHT.h>
#include <HX711.h>

//Constants
#define DHTPIN 10     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define DOUT 3
#define CLK 2
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
HX711 scale (DOUT,CLK);

float calibration_factor=12270;
//Variables
int chk;
float hum;  //Stores humidity value
float temp; //Stores temperature value
float weight;

// Configure software serial port
SoftwareSerial SIM900(7, 8); 

void setup() {
  Serial.begin(9600);
  Serial.println("init");  
  // Arduino communicates with SIM900 GSM shield at a baud rate of 19200
  // Make sure that corresponds to the baud rate of your module
  SIM900.begin(19200);
  // Give time to your GSM shield log on to network
  delay(20000);  

  dht.begin();
  scale.set_scale(12270); // calibration factor
  scale.tare();
  weight_measure;
  

}

void loop() {
}
void weight_measure(){
  Serial.println("Place object on the scale");
  delay(10000);
  weight=scale.get_units(100),3; //average of 100 measurements
  sensordata();
  
}
void sensordata(){
    delay(2500);
    //Read data and store it to variables hum and temp
    hum = dht.readHumidity();
    temp= dht.readTemperature();
    //Print temp and humidity values to serial monitor
    String message = String("Humidity: " + String(hum) + " %, Temp: " + String(temp) + " Celsius, Weight: " + String(weight));
    Serial.println(message);
    // Send the SMS
    
    sendSMS("6979023619", message, true); 
    
    delay(10000); //Delay 2 sec.
 }


void sendSMS(String phone, String message, bool actualSend) {

  if (actualSend){
    Serial.print("Sending SMS to ");  
    Serial.print(phone); 
    Serial.print(" (actual)"); 
        // AT command to set SIM900 to SMS mode1
    SIM900.print("AT+CMGF=1\r"); 
    delay(100);
  
    // REPLACE THE X's WITH THE RECIPIENT'S MOBILE NUMBER
    // USE INTERNATIONAL FORMAT CODE FOR MOBILE NUMBERS
    String code = String("AT + CMGS = \"+30" + phone + "\"");
    SIM900.println(code); 
//    SIM900.println("AT + CMGS = \"+30" + phone + "\""); 
    delay(100);
    
    // REPLACE WITH YOUR OWN SMS MESSAGE CONTENT
    SIM900.println(message); 
    delay(100);
  
    // End AT command with a ^Z, ASCII code 26
    SIM900.println((char)26); 
    delay(100);
    SIM900.println();
    // Give module time to send SMS
    delay(5000); 
    Serial.println("Sent");
  }
  else{
    Serial.print("Sending SMS to ");  
    Serial.print(phone); 
    Serial.println(" (virtual)");  
  }

}
